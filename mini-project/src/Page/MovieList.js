import React, { useContext, useEffect } from "react";
import axios from "axios";
import { MovieContext } from "../Component/MovieContext";

const MovieList = () => {
  const [listMovie, setListMovie, , , , , , setIdMovie] = useContext(MovieContext);

  const deleteMovie = (e) => {
    const id = parseInt(e.target.value);
    console.log(id);
    setListMovie(listMovie.filter((el) => el.id !== id));
    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${id}`)
      .then((res) => {
        console.log(res)
      });
  }

  const editMovie = (e) => {
    setIdMovie(e.target.value);
  }

  useEffect(() => {
    if (listMovie.length === 0) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/movies`)
        .then((res) => {
          const movie = res.data.map((el) => {
            return {
              id: el.id,
              rating: el.rating,
              title: el.title,
              description: el.description,
              duration: el.duration,
              genre: el.genre,
              year: el.year,
            };
          });
          movie.sort((a, b) =>
            a.rating < b.rating ? 1 : b.rating < a.rating ? -1 : 0
          );
          console.log(movie);
          setListMovie(movie);
        });
    }
  });
  return <>
    <section>
      <h1 align="center">Tabel Movie</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Tahun</th>
            <th>Durasi</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>Deskripsi</th>
            <th>Opsi</th>
          </tr>
        </thead>
        <tbody>
          {listMovie.map((el, i) => {
            return (
              <tr key={el.id}>
                <td>{i + 1}</td>
                <td>{el.title}</td>
                <td>{el.year}</td>
                <td>{el.duration} Menit</td>
                <td>{el.genre}</td>
                <td>{el.rating}/10</td>
                <td>{el.description}</td>
                <td>
                  <button value={el.id} onClick={editMovie}>
                    Edit
                    </button>{" "}
                  <button value={el.id} onClick={deleteMovie}>
                    Delete
                    </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </section>
  </>;
};

export default MovieList;
