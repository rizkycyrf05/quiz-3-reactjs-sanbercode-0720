import React, { useContext } from "react";
import { Link } from "react-router-dom";
import logo from "../Assets/img/logo.png";
import { MovieContext } from "../Component/MovieContext";

function Header() {
  const [, , , , isLoggin] = useContext(MovieContext);
  return (
    <>
      <header>
        <img id="logo" src={logo} width="200px" alt="Logo" />
        <nav>
          <ul>
            <li>
                <a><Link to="/">Home</Link></a>
            </li>
            <li>
                <a><Link to="/about">About</Link></a>
            </li>
            {isLoggin === true && (
              <li>
                <a><Link to="/movie-list">Movie List Editor</Link></a>
              </li>
            )}
            {isLoggin === false && (
              <li>
                <a><Link to="/login">Login</Link></a>
              </li>
            )}
          </ul>
        </nav>
      </header>
    </>
  );
}

export default Header;
