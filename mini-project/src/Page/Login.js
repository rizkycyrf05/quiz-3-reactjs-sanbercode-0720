import React, { useContext, useState } from "react";
import { MovieContext } from "../Component/MovieContext";

function Login() {
  const [, , user, , , setIsLoggin] = useContext(MovieContext);
  const [input, setInput] = useState({ username: "", password: "" });

  const handleSubmit = (e) => {
    e.preventDefault();
    if (input.username.replace(/\s/g, "") !== "" && input.password.replace(/\s/g, "") !== "") {
      if (user.username === input.username && user.password === input.password) {
        setIsLoggin(true);
      } else {
        alert(`Username & Password Yang Anda Masukkan Salah`);
      }
    } else {
      alert(`Form Masih Kosong`);
    }
  };
  const handleChange = (e) => {
    setInput({ ...input, [e.target.name]: e.target.value });
  };
  return (
    <>
      <section id="login">
        <h1 align="center">Login</h1>
        <form onSubmit={handleSubmit}>
          <div>
            <label>Username </label>
            <input
              type="text"
              value={input.username}
              onChange={handleChange}
              name="username"
            />
          </div>
          <div>
            <label>Password </label>
            <input
              type="password"
              value={input.password}
              onChange={handleChange}
              name="password"
            />
          </div>
          <button>Login</button>
        </form>
      </section>
    </>
  );
}

export default Login;
