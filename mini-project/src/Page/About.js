import React from "react";

function About() {
  return (
    <>
      <div className="about">
      <h1 align="center">Data Peserta Sanbercode Bootcamp Reactjs</h1>
      <ol type="number">
        <li>
          <b>Nama : </b>Nur Ridho Rizki
        </li>
        <li>
          <b>Email : </b>rizkycyrf05@gmail.com
        </li>
        <li>
          <b>Sistem Operasi yang digunakan : </b>Windows10
        </li>
        <li>
          <b>Akun Gitlab : </b>rizkycyrf05
        </li>
        <li>
          <b>Akun Telegram : </b>https://t.me/Rizky_Cyrf
        </li>
      </ol>
      </div>
    </>
  );
}

export default About;
