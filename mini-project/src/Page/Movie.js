import React from "react";
import MovieList from "./MovieList";
import MovieForm from "./MovieForm";
const Movie = () => {
  return (
    <>
      <MovieList />
      <MovieForm />
    </>
  );
};

export default Movie;
