import React from "react";
import { MovieProvider } from "./Component/MovieContext";
import Routes from "./Component/Routes";

function Quiz() {
  return (
    <>
      <MovieProvider>
        <Routes />
      </MovieProvider>
    </>
  );
}

export default Quiz;
