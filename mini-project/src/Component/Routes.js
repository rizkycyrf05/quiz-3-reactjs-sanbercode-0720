import React, { useContext } from "react";
import Footer from "../Page/Footer";
import Header from "../Page/Header";
import About from "../Page/About";
import Home from "../Page/Home";
import Movie from "../Page/Movie";
import Login from "../Page/Login";
import { Switch, Route, Redirect } from "react-router-dom";
import { MovieContext } from "./MovieContext";

function Routes() {
  const [, , , , isLoggin] = useContext(MovieContext);
  return (
    <>
      <Header />
      <div class="list">
        <Switch>
          <Route exact path="/about">
            <About />
          </Route>
          <Route exact path="/movie-list">
            {isLoggin === false ? <Redirect to="/" /> : <Movie />}
          </Route>
          <Route exact path="/login">
            {isLoggin ? <Redirect to="/" /> : <Login />}
          </Route>
          <Route exact path="/">
            <Home />
          </Route>
        </Switch>
      </div>
      <Footer />
    </>
  );
}

export default Routes;
