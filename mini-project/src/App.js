import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Quiz from "./Quiz";
import "../src/App.css";

function App() {
  return (
    <>
      <Router>
        <Quiz />
      </Router>
    </>
  );
}

export default App;

